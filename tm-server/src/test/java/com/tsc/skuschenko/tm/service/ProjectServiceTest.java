package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class ProjectServiceTest {


    @Test
    public void testChangeStatusById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        Assert.assertNotNull(project);
        projectService.changeStatusById(
                project.getUserId(), project.getId(), Status.COMPLETE
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(Status.COMPLETE.getDisplayName(), projectFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        projectService.changeStatusByName(
                project.getUserId(), project.getName(), Status.COMPLETE
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        projectService.completeById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        projectService.completeByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        Assert.assertNotNull(project.getName());
        projectService.completeById(project.getUserId(), project.getId());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final IProjectService
                projectService = testService();
        projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
    }

    @Test
    public void testFindOneById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind);
    }

    @NotNull
    private ProjectDTO testProjectModel() {
        @Nullable final ProjectDTO project = new ProjectDTO();
        project.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        project.setName("name1");
        project.setDescription("des1");
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("name1", project.getName());
        return project;
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        projectService.removeOneById(project.getUserId(), project.getId());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        projectService.removeOneByIndex(project.getUserId(), 0);
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        projectService.removeOneByName(
                project.getUserId(), project.getName()
        );
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNull(projectFind);
    }

    @NotNull
    private IProjectService testService() {
        @NotNull final IPropertyService propertyService =
                new PropertyService();
        @NotNull final IConnectionService connectionService =
                new ConnectionService(propertyService);
        Assert.assertNotNull(connectionService);
        @NotNull final IProjectService projectService =
                new ProjectService(connectionService);
        Assert.assertNotNull(projectService);
        return projectService;
    }

    @Test
    public void testStartById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        projectService.startById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1",
                "des1");
        projectService.startByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project = projectService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        projectService.startByName(project.getUserId(), "name1");
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        project.setName("name2");
        project.setDescription("des2");
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneById(project.getUserId(), project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

    @Test
    public void testUpdateOneByIndex() {
        @NotNull final IProjectService
                projectService = testService();
        @NotNull final ProjectDTO project =
                projectService.add(
                        "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                        "name1", "des1"
                );
        project.setName("name2");
        project.setDescription("des2");
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final ProjectDTO projectFind =
                projectService.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

}