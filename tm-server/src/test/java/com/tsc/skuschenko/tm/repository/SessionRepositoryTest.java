package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class SessionRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);
    @NotNull
    final SqlSession sqlSession =
            connectionService.getSqlConnection();

    @Test
    public void testClear() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        sessionRepository.removeSessionQuery(session);
        sessionRepository.updateSessionQuery(session);
        sqlSession.commit();
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        Assert.assertNull(sessionFind);
    }

    @Test
    public void testCreate() {
        @NotNull final SessionDTO session = testSessionModel();
        testRepository(session);
    }

    @Test
    public void testFindOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionRepository
                sessionRepository = testRepository(session);
        sessionRepository.removeSessionById(session);
        sqlSession.commit();
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        Assert.assertNull(sessionFind);
    }

    @NotNull
    private ISessionRepository testRepository(@NotNull final SessionDTO session) {
        @NotNull final ISessionRepository sessionRepository =
                sqlSession.getMapper(ISessionRepository.class);
        Assert.assertTrue(
                sessionRepository.findAll(session.getUserId()).isEmpty()
        );
        sessionRepository.add(session);
        sqlSession.commit();
        @Nullable final SessionDTO sessionById =
                sessionRepository.findSessionById(session);
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        return sessionRepository;
    }

    @NotNull
    private SessionDTO testSessionModel() {
        @Nullable final SessionDTO session = new SessionDTO();
        session.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        session.setId("Id1");
        session.setTimestamp(System.currentTimeMillis());
        String signature = SignatureUtil.sign(session, "password", 454);
        session.setSignature(signature);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("UId1", session.getUserId());
        Assert.assertEquals("Id1", session.getId());
        Assert.assertEquals(
                signature,
                session.getSignature()
        );
        return session;
    }

}
