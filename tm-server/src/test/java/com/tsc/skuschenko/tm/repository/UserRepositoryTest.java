package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class UserRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);
    @NotNull
    final SqlSession sqlSession =
            connectionService.getSqlConnection();

    @Test
    public void testCreate() {
        @NotNull final UserDTO user = testUserModel();
        testRepository(user);
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findByEmail(user.getEmail());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindOneById() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        userRepository.removeByLogin(user.getLogin());
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        Assert.assertNull(userFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserRepository
                userRepository = testRepository(user);
        userRepository.removeOneById(user.getId());
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        Assert.assertNull(userFind);
    }

    @NotNull
    private IUserRepository testRepository(@NotNull final UserDTO user) {
        @NotNull final IUserRepository userRepository =
                sqlSession.getMapper(IUserRepository.class);
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        @Nullable final UserDTO userById =
                userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        return userRepository;
    }

    @NotNull
    private UserDTO testUserModel() {
        @Nullable final UserDTO user = new UserDTO();
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setEmail("Email@Email.ru");
        user.setLastName("LastName");
        user.setLogin("Login");
        user.setPasswordHash(HashUtil.salt("secret", 35484, "password"));
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("FirstName", user.getFirstName());
        Assert.assertEquals("MiddleName", user.getMiddleName());
        Assert.assertEquals("LastName", user.getLastName());
        Assert.assertEquals("Login", user.getLogin());
        Assert.assertEquals("Email@Email.ru", user.getEmail());
        Assert.assertEquals(
                HashUtil.salt("secret", 35484, "password"),
                user.getPasswordHash()
        );
        return user;
    }

}
