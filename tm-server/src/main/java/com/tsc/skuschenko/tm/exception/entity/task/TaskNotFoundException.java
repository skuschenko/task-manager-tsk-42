package com.tsc.skuschenko.tm.exception.entity.task;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
