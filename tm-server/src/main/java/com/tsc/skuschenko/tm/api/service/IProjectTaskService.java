package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskByProject(
            @NotNull String userId, @Nullable String projectId,
            @Nullable String taskId);

    void clearProjects(@NotNull String userId);

    @Nullable
    ProjectDTO deleteProjectById(
            @NotNull String userId, @Nullable String projectId
    );

    @Nullable
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull String userId, @Nullable String projectId
    );

    @NotNull
    TaskDTO unbindTaskFromProject(
            @NotNull String userId, @Nullable String projectId,
            @Nullable String taskId
    );

}
