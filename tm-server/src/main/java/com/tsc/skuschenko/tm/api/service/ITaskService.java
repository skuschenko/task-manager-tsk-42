package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface ITaskService{

    @NotNull
    TaskDTO add(
            @Nullable String userId, @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO changeStatusById(
            @NotNull String userId, @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    TaskDTO changeStatusByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    TaskDTO changeStatusByName(
            @NotNull String userId, @Nullable String name,
            @Nullable Status status
    );

    void clear(@NotNull String userId);

    @NotNull
    TaskDTO completeById(
            @NotNull String userId, @Nullable String id
    );

    @NotNull
    TaskDTO completeByIndex(
            @NotNull String userId, @Nullable Integer index
    );

    @NotNull
    TaskDTO completeByName(@NotNull String userId,
                           @Nullable String name);

    @NotNull
    List<TaskDTO> findAll(
            @NotNull String userId,
            @Nullable Comparator<TaskDTO> comparator
    );

    void addAll(@Nullable List<TaskDTO> tasks);

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    List<TaskDTO> findAll();

    @SneakyThrows
    void clear();

    @Nullable
    TaskDTO findOneById(
            @NotNull String userId, @Nullable String id
    );

    @Nullable
    TaskDTO findOneByIndex(
            @NotNull String userId, Integer index
    );

    @Nullable
    TaskDTO findOneByName(
            @NotNull String userId, String name
    );

    @Nullable
    TaskDTO removeOneById(
            @NotNull String userId, String id
    );

    @Nullable
    TaskDTO removeOneByIndex(
            @NotNull String userId, Integer index
    );

    @Nullable
    TaskDTO removeOneByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    TaskDTO startById(
            @NotNull String userId, @Nullable String id
    );

    @NotNull
    TaskDTO startByIndex(
            @NotNull String userId, @Nullable Integer index
    );

    @NotNull
    TaskDTO startByName(
            @NotNull String userId, @Nullable String name
    );

    @NotNull
    TaskDTO updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    @NotNull
    TaskDTO updateOneByIndex(
            @NotNull String userId, @Nullable Integer index,
            @Nullable String name, @Nullable String description
    );

}
