package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.dto.SessionDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session" +
            "(id, timestamp, signature, user_id) " +
            "VALUES(#{id},#{timestamp},#{signature},#{user_id})")
    void add(@NotNull SessionDTO session);

    @Select("SELECT * FROM tm_session WHERE user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable List<SessionDTO> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT FROM tm_session WHERE id=#{id }")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
    })
    SessionDTO findSessionById(SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE id=#{id }")
    void removeSessionById(SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id=#{user_id }")
    void removeSessionQuery(SessionDTO session);

    @Update("UPDATE tm_session SET id=#{id}, timestamp=#{timestamp}, " +
            "signature=#{signature}, user_id=#{userId}")
    void updateSessionQuery(SessionDTO session);

}
