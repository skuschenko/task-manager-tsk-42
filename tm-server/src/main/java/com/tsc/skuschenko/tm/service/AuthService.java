package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyLoginException;
import com.tsc.skuschenko.tm.exception.empty.EmptyPasswordException;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (!Optional.ofNullable(roles)
                .filter(item -> item.length != 0).isPresent()) return;
        @NotNull final UserDTO user =
                Optional.ofNullable(getUser())
                        .orElseThrow(AccessDeniedException::new);
        @NotNull final String userRole =
                Optional.ofNullable(user.getRole())
                        .orElseThrow(AccessDeniedException::new);
        for (@NotNull final Role role : roles) {
            if (role.getDisplayName().equals(userRole))
                return;
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public UserDTO getUser() {
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(
            @Nullable final String login, @Nullable final String password
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable final Optional<UserDTO> userFind =
                Optional.ofNullable(userService.findByLogin(login));
        if (userFind.isPresent()) {
            @Nullable final String secret = propertyService.getPasswordSecret();
            @Nullable final Integer iteration
                    = propertyService.getPasswordIteration();
            @NotNull final UserDTO user = userFind.
                    filter(item -> item.getPasswordHash() != null &&
                            item.getPasswordHash().equals(
                                    HashUtil.salt(secret, iteration, password))
                            && !item.isLocked()
                    )
                    .orElseThrow(AccessDeniedException::new);
            userId = user.getId();
        }
    }

    @Override
    public void logout() {
        userId = null;
    }

    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

}
