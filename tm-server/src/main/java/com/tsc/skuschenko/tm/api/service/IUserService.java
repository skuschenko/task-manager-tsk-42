package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.dto.UserDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<UserDTO> {

    void clear();

    @Nullable
    @SneakyThrows
    List<UserDTO> findAll();

    void addAll(@Nullable List<UserDTO> users);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @Nullable String login, @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    UserDTO findById(@Nullable String email);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String login);

    boolean isLoginExist(@Nullable String login);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO updateUser(
            @Nullable String userId, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

}
