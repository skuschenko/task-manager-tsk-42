package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.service.*;
import com.tsc.skuschenko.tm.endpoint.AbstractEndpoint;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.service.*;
import com.tsc.skuschenko.tm.util.SystemUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String ENDPOINT_PATH =
            "com.tsc.skuschenko.tm.endpoint";

    @NotNull
    private static final String OPERATION_FAIL = "fail";

    @NotNull
    private static final String OPERATION_OK = "ok";

    @NotNull
    private static final String TM_PID = "task-manager-server.pid";

    @NotNull
    private final IDataService dataService =
            new DataService(this);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService =
            new SessionService(this, connectionService);

    @NotNull
    private final IProjectService projectService =
            new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService =
            new UserService(connectionService, propertyService);

    @NotNull
    private final IAuthService authService =
            new AuthService(userService, propertyService);

    private void createDefaultProjectAndTask(
            @NotNull final UserDTO user, @NotNull final String name,
            @NotNull String description
    ) {
        @NotNull final ProjectDTO project =
                projectService.add(user.getId(), name, description);
        project.setUserId(user.getId());
        @NotNull final TaskDTO task =
                taskService.add(user.getId(), name, description);
        task.setProjectId(project.getId());
        task.setUserId(user.getId());
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    public void init() {
        initUsers();
        initPID();
        initEndpoints();
    }

    private void initEndpoints() {
        @NotNull final Reflections reflections = new Reflections(ENDPOINT_PATH);
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        classes.stream().filter(item ->
                !Modifier.isAbstract(item.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .forEach(item -> {
                    try {
                        registry(item
                                .getDeclaredConstructor(IServiceLocator.class)
                                .newInstance(this)
                        );
                    } catch (InstantiationException
                            | IllegalAccessException
                            | InvocationTargetException
                            | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = TM_PID;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        @Nullable final UserDTO userTestFind =
                userService.findByLogin("test");
        if(!Optional.ofNullable(userTestFind).isPresent()) {
            @NotNull final UserDTO userTest =
                    userService.create("test", "test", "text@test.ru");
            createDefaultProjectAndTask(userTest, "Test", "Test");
        }
        @Nullable final UserDTO userAdminFind =
                userService.findByLogin("test");
        if(!Optional.ofNullable(userAdminFind).isPresent()) {
            @NotNull final UserDTO userAdmin =
                            userService.create("admin", "admin", Role.ADMIN);
            createDefaultProjectAndTask(userAdmin, "Admin", "Admin");
        }
    }

    private void registry(@Nullable final AbstractEndpoint endpoint) {
        if (!Optional.ofNullable(endpoint).isPresent()) return;
        @Nullable String host = propertyService.getServerHost();
        if (!Optional.ofNullable(host).isPresent()) return;
        @Nullable String port = propertyService.getServerPort();
        if (!Optional.ofNullable(port).isPresent()) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
