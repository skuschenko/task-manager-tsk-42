package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractBusinessEntity implements IWBS {

    @OneToMany(mappedBy = "project")
    @Nullable
    private List<Task> tasks = new ArrayList<>();

}
