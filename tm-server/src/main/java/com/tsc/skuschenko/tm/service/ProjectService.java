package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.add(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@Nullable final List<ProjectDTO> projects) {
        projects.forEach(item ->
                add(item.getUserId(), item.getName(), item.getDescription())
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;

        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        try {
            @NotNull final IProjectRepository projectRepository =
                    sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clearAllProjects();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            entityRepository.clear(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;

        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO completeByName(@NotNull final String userId,
                                     @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            return entityRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<ProjectDTO> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {

            return entityRepository.findAllWithUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            return entityRepository.findAllWithUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            return entityRepository.findOneById(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            return entityRepository.findOneByIndex(userId, index);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            return entityRepository.findOneByName(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.removeProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.removeProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.removeProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        @NotNull final ProjectDTO project = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.updateProjectQuery(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project = Optional.ofNullable(
                    findOneByName(userId, name)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project = Optional.ofNullable(
                    findOneById(userId, id)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final IProjectRepository entityRepository =
                sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final ProjectDTO project = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityRepository.updateProjectQuery(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
