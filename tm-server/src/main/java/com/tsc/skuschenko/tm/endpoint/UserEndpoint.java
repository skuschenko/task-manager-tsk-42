package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint {

    @Nullable
    private IUserService userService;

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @WebMethod
    @NotNull
    public UserDTO createUser(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login,
            @WebParam(name = "password", partName = "password")
            @Nullable final String password
    ) throws AccessForbiddenException {
        return userService.create(login, password);
    }

    @WebMethod
    @NotNull
    public UserDTO createUserWithEmail(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login,
            @WebParam(name = "password", partName = "password")
            @Nullable final String password,
            @WebParam(name = "email", partName = "email")
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @WebMethod
    @NotNull
    public UserDTO createUserWithRole(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login,
            @WebParam(name = "password", partName = "password")
            @Nullable final String password,
            @WebParam(name = "role", partName = "role")
            @Nullable final String role
    ) {
        return userService.create(login, password, Role.valueOf(role));
    }

    @WebMethod
    @Nullable
    public UserDTO findUserByEmail(
            @WebParam(name = "email", partName = "email")
            @Nullable final String email
    ) {
        return userService.findByEmail(email);
    }

    @WebMethod
    @Nullable
    public UserDTO findUserByLogin(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        return userService.findByLogin(login);
    }

    @WebMethod
    @Nullable
    public UserDTO getUser() {
        return serviceLocator.getAuthService().getUser();
    }

    public boolean isEmailExist(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        return userService.isEmailExist(login);
    }

    public boolean isLoginExist(
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        return userService.isLoginExist(login);
    }

    @WebMethod
    @NotNull
    public UserDTO lockUserByLogin(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    @Nullable
    public UserDTO removeUserByLogin(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.removeByLogin(login);
    }

    @WebMethod
    @NotNull
    public UserDTO setPassword(
            @WebParam(name = "userId", partName = "userId")
            @Nullable final String userId,
            @WebParam(name = "password", partName = "password")
            @Nullable final String password
    ) {
        return userService.setPassword(userId, password);
    }

    @WebMethod
    @NotNull
    public UserDTO unlockUserByLogin(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login")
            @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }

    @WebMethod
    @NotNull
    public UserDTO updateUser(
            @WebParam(name = "userId", partName = "userId")
            @Nullable final String userId,
            @WebParam(name = "firstName", partName = "firstName")
            @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName")
            @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName")
            @Nullable final String middleName
    ) {
        return userService.updateUser(userId, firstName, lastName, middleName);
    }

}
