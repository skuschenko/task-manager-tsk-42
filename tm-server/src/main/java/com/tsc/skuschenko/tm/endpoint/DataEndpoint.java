package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class DataEndpoint extends AbstractEndpoint {

    public DataEndpoint() {
        super(null);
    }

    public DataEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @SneakyThrows
    public void loadBackupCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadBackupCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBase64Command(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBase64Command();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBinaryCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBinaryCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJsonFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJsonJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXmlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXmlJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataYamlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataYamlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveBackupCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveBackupCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBase64Command(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBase64Command();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBinaryCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBinaryCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJsonFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJsonJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXmlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXmlJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataYamlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataYamlFasterXmlCommand();
    }

}