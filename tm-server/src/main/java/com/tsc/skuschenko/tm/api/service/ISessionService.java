package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @Nullable
    UserDTO checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull SessionDTO session) throws AccessForbiddenException;

    ISessionRepository getSessionRepository();

    @Nullable
    SessionDTO open(@NotNull String login, @NotNull String password);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session, @Nullable Role role)
            throws AccessForbiddenException;

    void validate(@Nullable SessionDTO session) throws AccessForbiddenException;

}
