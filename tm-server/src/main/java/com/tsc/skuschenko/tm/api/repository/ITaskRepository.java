package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.dto.TaskDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task" +
            "(id, dateBegin, dateEnd, description, name, user_id, status, " +
            "created,project_id) VALUES(#{id},#{dateStart},#{dateFinish}," +
            "#{description}, #{name},#{userId},#{status},#{created}," +
            "#{projectId})")
    void add(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE user_id =#{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_task")
    void clearAllTasks();

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable List<TaskDTO> findAll();

    @Select("SELECT * FROM tm_task WHERE project_id=#{projectId}" +
            "AND user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "project_id", column = "projectId"),
    })
    @Nullable
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    );

    @Select("SELECT * FROM tm_task WHERE user_id=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable List<TaskDTO> findAllWithUserId(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    TaskDTO findById(@NotNull String id);

    @Select("SELECT * FROM tm_task WHERE WHERE id = #{id} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    TaskDTO findOneById(@NotNull String userId, @NotNull String id);

    @Select("SELECT * FROM tm_task WHERE WHERE AND " +
            "user_id=#{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Select("SELECT * FROM tm_task WHERE WHERE name = #{name} AND " +
            "user_id=#{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created"),
            @Result(property = "projectId", column = "project_id"),
    })
    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @NotNull String name);

    @Delete("DELETE FROM tm_task WHERE id={id}")
    void remove(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE user_id  =#{userId }" +
            "AND id=#{taskId}")
    void removeOneById(@NotNull String userId, @NotNull String taskId);

    @Delete("DELETE FROM tm_task WHERE user_id  =#{userId }")
    void removeTaskQuery(@NotNull TaskDTO task);

    @Update("UPDATE tm_task SET id=#{id}, dateBegin=#{dateStart}, " +
            "dateEnd=#{dateFinish}, description=#{description}," +
            " name=#{name}, user_id=#{userId}, status=#{status}, " +
            "created=#{created},project_id=#{projectId}")
    void updateTaskQuery(@NotNull TaskDTO task);

}
